const mysql = require('mysql');

function createConnect(){
  return mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password:'',
    database:'test'
    });
} 

  
module.exports = {
  load:  (sql) => {
    //use async await to get data, return callback
    // let getInfo = async function(callback){
    //   const con = createConnect();
    //   con.connect();
    //   await con.query(sql, function(err, results, field){
    //     if (err) throw err;
    //    callback(results);
    //   });
    // }
    // return getInfo;
      
    return new Promise(function(resolve, reject){
      const con = createConnect();
      con.connect();
      con.query(sql, function(err, results){
        if (err) throw err;
        resolve(results);
      });
      con.end();
    })
  }
  
  



};