const db = require('../libs/db.mysql');
const tbName = 'products'

module.exports = {
  all: async () => {  
    const sql = `select * from ${tbName}`;
    const results = await db.load(sql);
    return results;
  },
  getById: async (id) => {
    const sql = `select * from ${tbName} where ProId = ${id}`
    const results = await db.load(sql);
    return results;
  },
  create: async (name, tinyDes, fullDes, price, quantity) => {
    const sql = `insert into ${tbName}(ProName, TinyDes, FullDes, Price, CatId, Quantity)
     values('${name}', '${tinyDes}', '${fullDes}', ${price}, ${6}, '${quantity}')`
     await db.load(sql);
  },
  changeById: async (id, name, tinyDes, fullDes, price, quantity) => {
    const sql = `update ${tbName}
    set ProName='${name}', TinyDes= '${tinyDes}', FullDes= '${fullDes}', Price= ${price}, Quantity=${quantity}
    where ProId = ${id}`;
    await db.load(sql);
  },

  deleteById: async (id) =>{
    const sql = `delete from ${tbName} where ProID=${id}`;
    await db.load(sql);
  }
}