const mProducts = require('../../models/products.model');

module.exports = {
  index: (req, res) => {
    res.render('home',{ title: 'Home'} );
  },
  showProducts: async (req, res) => {
    const products = await mProducts.all();
      res.render('products', { title: 'Products', products: products});
  }

}