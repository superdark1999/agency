const express = require('express');
const appController = require('./app.controller');
const router = express.Router();

router.get('/', appController.index);
router.get('/products', appController.showProducts);

module.exports = router;
