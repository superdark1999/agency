const express = require('express');
const router = express.Router();

const appRouter = require('./app/index');
const userRouter = require('./user/index');


router.use('/', appRouter);
router.use('/users', userRouter);


module.exports = router;