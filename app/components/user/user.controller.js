const mProducts = require('../../models/products.model');

module.exports = {
  index: (req, res) => {
    res.send('User home');
  },
  manageProducts: async (req, res) => {
    const products = await mProducts.all();
    res.render('users/manage-products', {
      title: 'Manage Products',
      products: products
    })
  },
  createProduct: (req, res) => {
    res.render('users/create-product', {title: 'Create product'});
  },
  pcreateProduct: async(req, res) => {
    await mProducts.create(
      req.body.product_name,
      req.body.product_description,
      req.body.product_description,
      10000000,
      req.body.available_quantity
    );
    res.redirect('/users/manage-products');
  },
  changeProduct: async (req, res) => {
    const product = await mProducts.getById(req.params.id);
    res.render('users/change-product', {
      title: "Change product",
      product: product
    });
  },
  pchangeProduct: async (req, res) => {
    await mProducts.changeById(
      req.params.id,
      req.body.product_name,
      req.body.product_description,
      req.body.product_description,
      10000000,
      req.body.available_quantity
    );
    res.redirect('/users/manage-products');
  },
  deleteProduct: async (req, res) => {
    await mProducts.deleteById(req.params.id);
    res.redirect('/users/manage-products');
  },
}

