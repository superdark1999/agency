const express = require('express');
const router = express.Router();

const userController = require('./user.controller');


router.get('/', userController.index);
router.get('/manage-products', userController.manageProducts);
router.get('/create-product', userController.createProduct);
router.post('/create-product', userController. pcreateProduct);
router.get('/change-product/:id', userController.changeProduct);
router.post('/change-product/:id', userController.pchangeProduct);
router.get('/delete-product/:id', userController.deleteProduct);


module.exports = router;