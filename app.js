const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

const  hbs = require('hbs');
hbs.registerPartials(__dirname + '/views/partials');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
 
const port= 3000;
            

app.use('/static', express.static('app/public'));
app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) 


app.use('', require('./app/components'));

app.listen(port, function(){
  console.log('Open server in port ' + port);
})